<?php
if (\Cake\Core\Configure::read('SESSION_DEBUG')) {
    $this->getRequest()->getSession()->write('SESSION_DEBUG', sprintf('%s%s',
        $this->getRequest()->getSession()->read('SESSION_DEBUG'), 'DefaultSkeletonDefaultLayout;'));
}
?>

<?php $this->extend('Core.default') ?>

<?php if(filter_var(getenv('PRINT_DEBUG'), FILTER_VALIDATE_BOOLEAN)){ ?>
    <h3>Default skeleton default layout</h3>
<?php } ?>

<?= $this->fetch('content') ?>

    <script>
        if (typeof webroot === 'undefined') {
            var webroot = '<?= \Cake\Routing\Router::url('/', true)?>';
        }
    </script>

<?php if (filter_var($cookieHelper->read('cookiePolicyConfirmed'), FILTER_VALIDATE_BOOLEAN) !== true) {

    $options = \Cake\Core\Configure::read('CookiePolicy')['options'] ?? [];

    if(!in_array('ignoreDefaultStyling', $options, true)){
        echo $this->Html->css('DefaultSkeleton.cookiePolicy');
    }

    if(!in_array('ignoreDefaultJs', $options, true)){
        echo $this->Html->script('DefaultSkeleton.cookiePolicy');
    }

    ?>

    <div id="cookie-policy">
        <div><?= \Cake\Core\Configure::read('CookiePolicy')['text'] ?></div>
        <button>Close</button>
    </div>

<?php } ?>