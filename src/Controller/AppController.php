<?php

declare(strict_types=1);

namespace DefaultSkeleton\Controller;

use Cake\Core\Configure;
use Core\Controller\AppController as BaseController;

/**
 * Class AppController
 * @package DefaultSkeleton\Controller
 */
class AppController extends BaseController
{
    public function initialize()
    {
        parent::initialize();

        if (Configure::read('PRINT_DEBUG')) {
            debug('DefaultSkeletonController');
        }

        if (Configure::read('SESSION_DEBUG')) {
            $this->getRequest()->getSession()->write('SESSION_DEBUG', sprintf(
                '%s%s',
                $this->getRequest()->getSession()->read('SESSION_DEBUG'),
                'DefaultSkeletonController;'
            ));
        }
    }
}
