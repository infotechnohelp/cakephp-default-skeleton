<?php

declare(strict_types=1);

namespace DefaultSkeleton\Controller\Api;

use Core\Controller\Api\AppController as BaseController;

/**
 * Class AppController
 * @package DefaultSkeleton\Controller\Api
 */
class AppController extends BaseController
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}
