<?php

namespace DefaultSkeleton\Controller\Api;

use App\Controller\Api\AppController;

class CookiePolicyController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['confirm']);
    }

    public function confirm()
    {
        $this->Cookie->configKey('cookiePolicyConfirmed', [
            'expires' => '1 month',
        ]);

        $this->Cookie->write('cookiePolicyConfirmed', true);

        $this->_setResponse([true]);
    }
}
